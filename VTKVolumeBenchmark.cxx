/*
 * Volume rendering benchmark code
 */

#include <iostream>

// VTK includes
#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>
#include <vtkDICOMImageReader.h>
#include <vtkGPUVolumeRayCastMapper.h>
#include <vtkImageData.h>
#include <vtkNew.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTimerLog.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>

int main (int, char**)
{
  vtkNew<vtkDICOMImageReader> reader;
  reader->SetDirectoryName("./Data");
  reader->Update();

  vtkImageData* im = reader->GetOutput();
  double range[2];
  im->GetScalarRange(range);
  int dims[3], extent[6];
  double spacing[3], origin[3];
  im->GetDimensions(dims);
  im->GetExtent(extent);
  im->GetSpacing(spacing);
  im->GetOrigin(origin);

  std::cout << "Dimensions: " << dims[0] << " " <<
    dims[1] << " " << dims[2] << std::endl;
  std::cout << "Origin: " << origin[0] << " " <<
    origin[1] << " " << origin[2] << std::endl;
  std::cout << "Spacing: " << spacing[0] << " " <<
    spacing[1] << " " << spacing[2] << std::endl;


  double avgSpacing = (spacing[0] + spacing[1] + spacing[2]) / 3.0;

  std::cout << "Average Spacing: " << avgSpacing << std::endl;

  std::cout << "Range: (" << range[0] << ", " << range[1] << ")" << std::endl;

  vtkNew<vtkGPUVolumeRayCastMapper> mapper;
  mapper->SetInputConnection(reader->GetOutputPort());
  mapper->SetAutoAdjustSampleDistances(0);

  vtkNew<vtkVolumeProperty> property;
  property->ShadeOn();

  vtkNew<vtkColorTransferFunction> ctf;
  vtkNew<vtkPiecewiseFunction> pf;
  property->SetColor(ctf.GetPointer());
  property->SetScalarOpacity(pf.GetPointer());

  vtkNew<vtkVolume> volume;
  volume->SetMapper(mapper.GetPointer());
  volume->SetProperty(property.GetPointer());

  for (int k = 0; k < 4; ++k)
    {
    switch (k)
      {
      case 0:
        {
        // First testing with tissue transfer function and
        std::cout << std::endl << "***************************" << std::endl;
        std::cout << "Tissue transfer function" << std::endl;

        ctf->RemoveAllPoints();
        ctf->AddRGBPoint(range[0], 0.0, 0.0, 0.0);
        ctf->AddRGBPoint(324.13, 1.0, 0.2, 0.2);
        ctf->AddRGBPoint(510.14, 1.0, 0.2, 0.2);
        ctf->AddRGBPoint(696.15, 0.9, 0.9, 0.9);
        ctf->AddRGBPoint(714.75, 1.0, 1.0, 1.0);
        ctf->AddRGBPoint(785.43, 1.0, 1.0, 1.0);
        ctf->AddRGBPoint(range[1], 1.0, 1.0, 1.0);

        pf->RemoveAllPoints();
        pf->AddPoint(range[0], 0.0);
        pf->AddPoint(324.13, 0.0);
        pf->AddPoint(510.14, 0.5);
        pf->AddPoint(696.15, 0.5);
        pf->AddPoint(714.75, 0.7);
        pf->AddPoint(785.43, 1.0);
        pf->AddPoint(range[1], 1.0);
        break;
        }
      case 1:
        {
        // Next most voxels translucent transfer function
        std::cout << std::endl << "***************************" << std::endl;
        std::cout << "Translucent transfer function" << std::endl;

        ctf->RemoveAllPoints();
        ctf->AddRGBPoint(-2048, 0, 0, 0);
        ctf->AddRGBPoint(-168.62, 0, 0, 0);
        ctf->AddRGBPoint(35.92, 0.5, 0.4, 0.18);
        ctf->AddRGBPoint(155.19, 0.61, 0.35, 0.07);
        ctf->AddRGBPoint(418.28, 0.84, 0.015, 0.15);
        ctf->AddRGBPoint(847.79, 0.75, 0.75, 0.75);
        ctf->AddRGBPoint(3592.73, 1, 1, 1);

        pf->RemoveAllPoints();
        pf->AddPoint(-2048, 0);
        pf->AddPoint(-168.62, 0);
        pf->AddPoint(35.92, 0.03);
        pf->AddPoint(155.19, 0.1);
        pf->AddPoint(418.28, 0.6);
        pf->AddPoint(847.79, 0.93);
        pf->AddPoint(3592.73, 0.84);
        break;
        }
      case 2:
        {
        // Next skin transfer function
        std::cout << std::endl << "***************************" << std::endl;
        std::cout << "Skin transfer function" << std::endl;

        ctf->RemoveAllPoints();
        ctf->AddRGBPoint(-2048, 0, 0, 0);
        ctf->AddRGBPoint(-168.62, 0, 0, 0);
        ctf->AddRGBPoint(35.92, 0.5, 0.4, 0.18);
        ctf->AddRGBPoint(155.19, 0.61, 0.35, 0.07);
        ctf->AddRGBPoint(418.28, 0.84, 0.015, 0.15);
        ctf->AddRGBPoint(847.79, 0.75, 0.75, 0.75);
        ctf->AddRGBPoint(3592.73, 1, 1, 1);

        pf->RemoveAllPoints();
        pf->AddPoint(-2048, 0);
        pf->AddPoint(-168.62, 0);
        pf->AddPoint(35.92, 1.00);
        pf->AddPoint(155.19, 0.1);
        pf->AddPoint(418.28, 0.6);
        pf->AddPoint(847.79, 0.93);
        pf->AddPoint(3592.73, 0.84);
        break;
        }
      case 3:
        {
        // transfer function with all scalar values having opacity 1.0
        std::cout << std::endl << "***************************" << std::endl;
        std::cout << "All opaque transfer function" << std::endl;

        ctf->RemoveAllPoints();
        ctf->AddRGBPoint(-1024, 0, 0.1, 0.2);
        ctf->AddRGBPoint(-168.62, 0.3, 0.2, 0);
        ctf->AddRGBPoint(35.92, 0.5, 0.4, 0.18);
        ctf->AddRGBPoint(155.19, 0.61, 0.35, 0.07);
        ctf->AddRGBPoint(418.28, 0.84, 0.015, 0.15);
        ctf->AddRGBPoint(847.79, 0.75, 0.75, 0.75);
        ctf->AddRGBPoint(3592.73, 1, 1, 1);

        pf->RemoveAllPoints();
        pf->AddPoint(-1024, 1);
        pf->AddPoint(2721, 1);
        break;
        }
      }
    vtkNew<vtkRenderer> renderer;
    renderer->AddVolume(volume.GetPointer());
    renderer->SetBackground(0.3, 0.4, 0.4);

    vtkNew<vtkRenderWindow> renWin;
    renWin->SetSize(1920, 1080);
    renWin->AddRenderer(renderer.GetPointer());
    renderer->GetActiveCamera()->Elevation(-90);
    renderer->ResetCamera();
    //renderer->GetActiveCamera()->Azimuth(90);

    double startTime = vtkTimerLog::GetUniversalTime();
    renWin->Render();
    std::cout << "First frame time: " <<
      vtkTimerLog::GetUniversalTime() - startTime << std::endl;

    double targetSpacing = avgSpacing * 0.25;
    for (int j = 0; j < 4; ++j)
      {
      targetSpacing = j == 0 ? targetSpacing : targetSpacing * 2;
      mapper->SetSampleDistance(targetSpacing);
      std::cout << std::endl << "Sample distance: " << targetSpacing << std::endl;
      for (int i = 0; i < 90; ++i)
        {
        renderer->GetActiveCamera()->Azimuth(2.0);
        startTime = vtkTimerLog::GetUniversalTime();
        renWin->Render();
        std::cout << "Frame time: " <<
          vtkTimerLog::GetUniversalTime() - startTime << std::endl;
        }
      }
//  vtkNew<vtkRenderWindowInteractor> iren;
//  iren->SetRenderWindow(renWin.GetPointer());
//
//  iren->Start();
    }

  return EXIT_SUCCESS;
}
